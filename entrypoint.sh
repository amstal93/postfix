#!/bin/sh

# generate configuration files
DOLLAR='$' envsubst \
  < /app/templates/mail-pam-pgsql.conf \
  > /etc/mail-pam-pgsql.conf
DOLLAR='$' envsubst \
  < /app/templates/mailboxes.cf \
  > /etc/postfix/mailboxes.cf
DOLLAR='$' envsubst \
  < /app/templates/aliases.cf \
  > /etc/postfix/aliases.cf
DOLLAR='$' envsubst \
  < /app/templates/main.cf \
  > /etc/postfix/main.cf

newaliases

syslogd
saslauthd -d -a pam -c -r -m /var/spool/postfix/var/run/saslauthd &
postfix start-fg
